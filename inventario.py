#!/usr/bin/env python3

# Diccionario vacío para almacenar los productos
inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    # Crear un diccionario con la información del producto y almacenarlo en el inventario usando el código como clave
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    # Iterar sobre el inventario y mostrar la información de cada producto
    for codigo, valores in inventario.items():
        nombre = valores['nombre']
        precio = valores['precio']
        cantidad = valores['cantidad']
        print(f'{codigo}: {nombre}, precio: {precio}, cantidad: {cantidad}')


def consultar(codigo: str):
    # Verificar si el código está en el inventario y mostrar la información del producto
    if codigo in inventario:
        valores = inventario[codigo]
        nombre = valores['nombre']
        precio = valores['precio']
        cantidad = valores['cantidad']
        print(f'{codigo}: {nombre}, precio: {precio}, cantidad: {cantidad}')


def agotados():
    # Mostrar los productos que tienen una cantidad igual a cero
    for codigo, canti in inventario.items():
        if canti['cantidad'] == 0:
            print(f'El artículo {codigo} se ha agotado')


def pide_articulo() -> (str, str, float, int):
    # Solicitar al usuario la información de un nuevo artículo y devolverla como una tupla
    codigo = str(input('Código de artículo: '))
    nombre = str(input('Nombre: '))
    precio = float(input('Precio: '))
    cantidad = int(input('Cantidad: '))
    return codigo, nombre, precio, cantidad


def menu() -> int:
    # Mostrar el menú y solicitar al usuario una opción
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")
    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            codigo, nombre, precio, cantidad = pide_articulo()
            insertar(codigo, nombre, precio, cantidad)
        elif opcion == '2':
            listar()
        elif opcion == '3':
            codigo_c = str(input('Código del artículo a consultar: '))
            consultar(codigo_c)
        elif opcion == '4':
            agotados()
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()

